#include <Wire.h>
#include <Adafruit_PWMServoDriver.h>

#include "GladPWM.h"

GladPWM pwm(1000);

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  Serial.println("start");
  pwm.init();
}

void loop() {
  
  Serial.println("start");
  
for (uint16_t i=0;i<16;++i)
{
  pwm.setState(i, 500);
}
Serial.println("current1");
for (uint16_t i=0;i<16;++i)
{
  Serial.println(pwm.getState(i));
}
delay(5000);

pwm.off(4);
pwm.off(8);
pwm.off(9);
Serial.println("current2");
for (uint16_t i=0;i<16;++i)
{
  Serial.println(pwm.getState(i));
}
delay(5000);


pwm.offAll();
Serial.println("currentOff");
for (uint16_t i=0;i<16;++i)
{
  Serial.println(pwm.getState(i));
}


Serial.println("OldState");
for (uint16_t i=0;i<16;++i)
{
  Serial.println(pwm.getOldState(i));
}
delay(5000);

pwm.restablishAll();
Serial.println("restablish");
for (uint16_t i=0;i<16;++i)
{
  Serial.println(pwm.getState(i));
}
delay(5000);

pwm.onAll();
Serial.println("onAll");
for (uint16_t i=0;i<16;++i)
{
  Serial.println(pwm.getState(i));
}
delay(5000);








}

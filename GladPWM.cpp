#include "GladPWM.h"
#include <Arduino.h>

GladPWM::GladPWM(uint16_t PWMfreq, uint32_t wireClock, uint8_t i2caddr)
{
    _PWMfreq = PWMfreq;
    _wireClock = wireClock;
    _i2caddr = i2caddr;
}
void GladPWM::init()
{
    Wire.setClock(_wireClock);
    _pwm = Adafruit_PWMServoDriver(_i2caddr);
    _pwm.begin();
    //pwm.reset();
    _pwm.setPWMFreq(_PWMfreq);
    _valueMinPWM = 30;
    _valueMaxPWM = 4095;

}

void GladPWM::on(uint8_t pin, uint8_t restablishState = 0)
{
    if (restablishState && _oldPWM[pin])
    {
        _currentPWM[pin] = _oldPWM[pin];
    }
    else
    {
        _currentPWM[pin] = _modes[1];
    }

    _pwm.setPWM(pin, 0, _currentPWM[pin]);
}

void GladPWM::off(uint8_t pin, boolean saveState = 0)
{
   if(saveState)
   {
       if(_currentPWM[pin])
       {
            _oldState[pin] = 1;
       }
       else
       {
           _oldState[pin] = 0;
       }
   }
   if (_currentPWM[pin])
   {
       _oldPWM[pin] = _currentPWM[pin];
       _currentPWM[pin] = 0;
       _pwm.setPWM(pin, 0, _currentPWM[pin]);
   }

}

void GladPWM::setState(uint8_t pin, uint16_t valuePWM)
{
    if(valuePWM > _valueMaxPWM)
    {
        valuePWM = _valueMaxPWM;
    }

    /*if(valuePWM!=0 && valuePWM < _valueMinPWM)
    {
        valuePWM = _valueMinPWM;
    }
*/


    _currentPWM[pin] = valuePWM;
    _pwm.setPWM(pin, 0, valuePWM);
}

void GladPWM::up(uint8_t pin, uint16_t val = 1)
{
    //��������� �������, ���� ����������� � ������� �����
    if (val == 5000)
    {
        if(_currentPWM[pin]<50)
        {
            GladPWM::setState(pin, _currentPWM[pin]+1);
        }else
            if(_currentPWM[pin]<1000)
            {
                GladPWM::setState(pin, _currentPWM[pin]+5);
            }else
            {
                GladPWM::setState(pin, _currentPWM[pin]+15);
            }

    } else if(_currentPWM < _valueMaxPWM)
    {
        GladPWM::setState(pin, _currentPWM[pin]+val);
    }

}
void GladPWM::down(uint8_t pin, uint16_t val = 1)
{
    if(_currentPWM[pin]==_valueMinPWM && val != 1) return;
    //��������� �������, ���� ����������� � ������� �����
    if (val == 5000)
    {
        if(_currentPWM[pin]<50)
        {
            GladPWM::setState(pin, _currentPWM[pin]-1);
        }else
            if(_currentPWM[pin]<1000)
            {
                GladPWM::setState(pin, _currentPWM[pin]-5);
            }else
            {
                GladPWM::setState(pin, _currentPWM[pin]-15);
            }

    } else if((_currentPWM > _valueMinPWM) && (val < _valueMaxPWM) && (_currentPWM - val < _valueMaxPWM))
    {
        GladPWM::setState(pin, _currentPWM[pin]-val);
    }
    else if(val >= _currentPWM)
    {
        GladPWM::setState(pin, 0);
    }
    else
    {
        setMin(pin);
    }
}

void GladPWM::setMaxAll()
{
    for(uint8_t i = 0; i<_amountChannels; ++i)
    {
        GladPWM::setMax(i);
    }
}
void GladPWM::setMinAll()
{
    for(uint8_t i = 0; i<_amountChannels; ++i)
    {
        GladPWM::setMin(i);
    }
}



void GladPWM::offAll()
{
    for(uint8_t i = 0; i<_amountChannels; ++i)
    {
        GladPWM::off(i, true);
    }
}

void GladPWM::onAll()
{
    for(uint8_t i = 0; i<_amountChannels; ++i)
    {
        GladPWM::on(i);
    }
}
void GladPWM::restablishAll()
{
    for(uint8_t i = 0; i<_amountChannels; ++i)
    {
        if(_oldState[i])
        {
            GladPWM::on(i);
        }
    }
}



void GladPWM::setMin(uint8_t pin)
{
    GladPWM::setState(pin, _valueMinPWM);
}

void GladPWM::setMax(uint8_t pin)
{
    GladPWM::setState(pin, _valueMaxPWM);
}


void GladPWM::changeMode(uint8_t pin)
{
    if (_amountModes >1)
    {
        for(int i=_amountModes-1; i>=0; --i)
        {
            if((i == (_amountModes-1) && _currentPWM[pin] >= _modes[i]) || _currentPWM[pin] == 0)
            {
                setState(pin, _modes[0]);
                break;
            }

            if (_currentPWM[pin] >= _modes[i])
            {
                setState(pin, _modes[i+1]);
                break;
            }
        }
    }
}




uint16_t GladPWM::getState(uint8_t i)
{
    return _currentPWM[i];
}
uint16_t GladPWM::getOldPWM(uint8_t i)
{
    return _oldPWM[i];
}
boolean GladPWM::getOldState(uint8_t i)
{
    return _oldState[i];
}



void GladPWM::begin(uint8_t prescale = 0)
{
    _pwm.begin(prescale = 0);
}
void GladPWM::reset()
{
    _pwm.reset();
}
void GladPWM::sleep()
{
    _pwm.sleep();
}
void GladPWM::wakeup()
{
    _pwm.wakeup();
}
void GladPWM::setExtClk(uint8_t prescale)
{
    _pwm.setExtClk(prescale);
}
void GladPWM::setPWMFreq(float freq)
{
    _PWMfreq = freq;
    _pwm.setPWMFreq(_PWMfreq);
}
uint8_t GladPWM::getPWM(uint8_t num)
{
    _pwm.getPWM(num);
}
void GladPWM::setPWM(uint8_t num, uint16_t on, uint16_t off)
{
    _pwm.setPWM(num, on, off);
}
void GladPWM::setPin(uint8_t num, uint16_t val, bool invert=false)
{
    _pwm.setPin(num, val, invert);
}















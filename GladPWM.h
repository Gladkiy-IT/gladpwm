#ifndef GladPWM_h
#define GladPWM_h
#include <Arduino.h>

#include <Wire.h>
#include <Adafruit_PWMServoDriver.h>

/*
#pragma pack(push,1)
typedef struct
{

} ;
#pragma pack(pop)
*/

class GladPWM
{
  public:
   // GladPWM(uint8_t pin);

	GladPWM(uint16_t PWMfreq = 500, uint32_t wireClock = 400000, uint8_t i2caddr = PCA9685_I2C_ADDRESS);
	//Adafruit_PWMServoDriver(TwoWire *I2C = &Wire, uint8_t addr = 0x40);
    void init();

	void on(uint8_t pin, uint8_t restablishState = 0);
	void off(uint8_t pin, boolean saveState = 0);
	void setState(uint8_t pin, uint16_t valuePWM);

	void offAll();
	void onAll();
	void restablishAll();

    void setMin(uint8_t pin);
    void setMax(uint8_t pin);
    void changeMode(uint8_t pin);


    void setMinAll();
    void setMaxAll();

    void up(uint8_t pin, uint16_t val = 1);
    void down(uint8_t pin, uint16_t val = 1);


	uint16_t getState(uint8_t i);
	uint16_t getOldPWM(uint8_t i);
	bool getOldState(uint8_t i);



  void begin(uint8_t prescale = 0);
  void reset();
  void sleep();
  void wakeup();
  void setExtClk(uint8_t prescale);
  void setPWMFreq(float freq);
  uint8_t getPWM(uint8_t num);
  void setPWM(uint8_t num, uint16_t on, uint16_t off);
  void setPin(uint8_t num, uint16_t val, bool invert=false);

  private:

    uint8_t const _amountChannels = 16;

    uint16_t _currentPWM[16] = {0};
    uint16_t _oldPWM[16] = {0};
    bool _oldState[16] = {0};
    uint16_t _valueMinPWM = 3;
    uint16_t _valueMaxPWM = 4095;

    const uint8_t _amountModes = 5;
    uint16_t _modes[5] = {30, 100, 700, 2048, 4095};

    Adafruit_PWMServoDriver _pwm;

    uint16_t _PWMfreq;
    uint32_t _wireClock;
    uint8_t _i2caddr = PCA9685_I2C_ADDRESS;


};



#endif
